console.log('hola')
const state = {
    display: 'location',
    weatherUrl: 'http://api.weatherstack.com/current?access_key=',
    weatherUrlLastPart: '&query=',
    activityUrl: 'http://www.boredapi.com/api/activity?type=',
    weatherUrlComplete: null,
    activityUrlComplete: null,
    weather: {
        city: null,
        country: null,
        temperature: null,
        typeWeather: null,
        weatherImage: null,
    },
    activity: [],
    location: null,
    showPopupfail: null,
    citiesForSearch: [
        'Bilbao',
        'Barcelona',
        'Madrid',
        'Rabat',
        'Caracas',
        'Edimburgo',
        'Paris',
        'London',
        'Marrakech',
        'Ouarzazate',
        'Santiago',
        'Krasnodar',
        'Oymyakon',
        'Managua',
        'Argel',
        'Kuala Lumpur',
        'Portugalete',
        'Sydney',
    ],
    cityName: '',
    typeActivity: null,
    apiKey: '0a8b7234f3fee003128b3cbed7c15054',
    doneActivities: [''],
    badDay: [
        'Light Rain',
        'Moderate or Heavy Rain Shower',
        'Rain Shower',
        'Thunder',
    ],
    goodDay: ['Clear', 'Sunny', 'Partly cloudy'],
    averageDay: [
        'Patchy Rain Possible',
        'Patchy Light Drizzle',
        'Overcast',
        'Haze',
        'Light Rain, Mist',
    ],
    weatherData: null,
    activityData: null,
    numberActivities: 2,
}

var body = document.getElementById('body')
var buttonActivity = document.getElementById('buttonGiveActivity')
var buttonLocation = document.getElementById('buttonSearchlocation')
var buttonReset = document.getElementById('buttonBackToLocation')
var buttonAnotherAct = document.getElementById('buttonOtherActivities')

body.addEventListener('load', renderCitiesInInput())
buttonLocation.addEventListener('click', changeStateCallApiWeather)
buttonActivity.addEventListener('click', changeStateCallApiActivity)
buttonAnotherAct.addEventListener('click', changeStateCallApiActivity)
buttonReset.addEventListener('click', resetGoIni)

function changeState(state, stateTochange) {
    state.display = stateTochange
}

function showScreens(state) {
    if (state.display === 'location') {
        document.getElementById('selectLocation').style.display = 'block'
        document.getElementById('screenWeather').style.display = 'none'
        document.getElementById('screenActivity').style.display = 'none'
    }
    if (state.display === 'weather') {
        document.getElementById('selectLocation').style.display = 'none'
        document.getElementById('screenWeather').style.display = 'block'
        document.getElementById('screenActivity').style.display = 'none'
    }
    if (state.display === 'activity') {
        document.getElementById('selectLocation').style.display = 'none'
        document.getElementById('screenWeather').style.display = 'none'
        document.getElementById('screenActivity').style.display = 'block'
    }
}

function showPopupfail() {
    var popup = document.getElementById('popUp')
    popup.classList.toggle('show')
}

async function changeStateCallApiWeather() {
    changeState(state, 'weather')
    getInputlocation(state)
    createWeatherUrl(state)
    await callWeather(state)
    if (state.weatherData === undefined) {
        showPopupfail()
    } else {
        weatherDataIntoVariable(state)
        printWeatherData(state)
        showScreens(state)
    }
}

function getInputlocation(state) {
    state.location = document.getElementById('boxLocation').value
}

function createWeatherUrl(state) {
    state.weatherUrlComplete =
        state.weatherUrl +
        state.apiKey +
        state.weatherUrlLastPart +
        state.location
    console.log(state.weatherUrlComplete)
    return state.weatherUrlComplete
}

async function callWeather(state) {
    state.weather = await fetch(state.weatherUrlComplete)
    state.weatherData = await state.weather.json()
    console.log('mamama ' + state)
}

function weatherDataIntoVariable(state) {
    state.weather.weatherImage = state.weatherData.current.weather_icons
    state.weather.typeWeather = state.weatherData.current.weather_descriptions
    state.weather.country = state.weatherData.location.country
    state.weather.city = state.weatherData.location.name
    state.weather.temperature = state.weatherData.current.temperature
}

function printWeatherData() {
    console.log(state.weatherData)
    document.getElementById('weatherImage').innerHTML =
        "<img src='" + state.weather.weatherImage + "' width='30px'>"
    document.getElementById('weatherDescription').innerHTML =
        state.weather.typeWeather
    document.getElementById('currentCountry').innerHTML = state.weather.country
    document.getElementById('currentCity').innerHTML = state.weather.city
    document.getElementById('temperature').innerHTML =
        state.weather.temperature + '°C'
}

async function changeStateCallApiActivity() {
    changeState(state, 'activity')
    transformWeatherData(state)
    createActivityUrl(state)
    i = 0
    while (i < state.numberActivities && state.activity.length < 15) {
        console.log('Length de la movida esta: ' + state.activity.length)
        await callActivity(state)
        let isRepeat = registerActivityKey(state)
        if (isRepeat === false) {
            i++
        }
    }
    if (state.activity.length === 14) {
        buttonAnotherAct.disabled = true
        document.getElementById('manyActivities').style.display = 'block'
    } else {
        buttonAnotherAct.disabled = false
    }

    printActivityData(state)
    showScreens(state)
}

function transformWeatherData(state) {
    console.log(state.weather.typeWeather)
    console.log('tranformadatostiempo?')

    if (
        state.weather.typeWeather[0] === state.badDay[0] ||
        state.weather.typeWeather[0] === state.badDay[1] ||
        state.weather.typeWeather[0] === state.badDay[2] ||
        state.weather.typeWeather[0] === state.badDay[3]
    ) {
        state.typeActivity = 'cooking&type=music&type=busywork'
    } else if (
        state.weather.typeWeather[0] === state.goodDay[0] ||
        state.weather.typeWeather[0] === state.goodDay[1] ||
        state.weather.typeWeather[0] === state.goodDay[2]
    ) {
        state.typeActivity = 'social&type=recreational&type=relaxation'
    } else if (
        state.weather.typeWeather[0] === state.averageDay[0] ||
        state.weather.typeWeather[0] === state.averageDay[1] ||
        state.weather.typeWeather[0] === state.averageDay[2] ||
        state.weather.typeWeather[0] === state.averageDay[3] ||
        state.weather.typeWeather[0] === state.averageDay[4]
    ) {
        state.typeActivity = 'education&type=charity&type=diy'
    } else {
        state.typeActivity = 'diy&type=charity&type=busywork'
    }
    console.log(state.typeActivity)
}

function createActivityUrl(state) {
    state.activityUrlComplete = state.activityUrl + state.typeActivity
    console.log(state.activityUrlComplete)
}

async function callActivity() {
    let data = await fetch(state.activityUrlComplete)
    state.activityData = await data.json()
}

function registerActivityKey(state) {
    console.log('maria')
    console.log(state.activityData.key)
    let arrayprueba = state.activity.filter(
        act => state.activityData.key === act.key
    )
    if (arrayprueba.length === 0) {
        let activity = {
            name: state.activityData.activity,
            key: state.activityData.key,
        }
        state.activity.push(activity)
        return false
    }
    return true
}

function printActivityData(state) {
    let nameActivity1 = document.getElementById('activity1')
    let nameActivity2 = document.getElementById('activity2')
    nameActivity1.innerHTML = state.activity[state.activity.length - 2].name
    nameActivity2.innerHTML = state.activity[state.activity.length - 1].name
}

function renderCitiesInInput() {
    var select = document.getElementById('boxLocation')
    var options = state.citiesForSearch
    for (var i = 0; i < options.length; i++) {
        var opt = options[i]
        var el = document.createElement('option')
        el.textContent = opt
        el.value = opt
        select.appendChild(el)
    }
}

function resetGoIni(state) {
    changeState(state, 'location')
    showScreens(state)
}

try {
    module.exports = {
        createActivityUrl,
        callActivity,
        getActivity,
        createWeatherUrl,
        callWeather,
        getCityName,
        showScreens,
        printCityArray,
        changeState,
        getInputlocation,
        getTypeWeather,
        renderCitiesInInput,
        transformWeatherData,
        printActivityData,
        weatherDataIntoVariable,
        resetGoIni,
        deleteDoneActivities,
        registerActivityKey,
    }
} catch {}

// module.exports = { createActivityUrl, callActivity, getActivity. callWeather, showScreenActivity, showScreenLocation, showScreenWeather
# C  I  T  Y  Z  E  N  
 
#### S t a y  A c t i v a t e 

### Comenzando 🚀

Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas.


### Pre-requisitos 📋

**1. Instala el Prettier en tu operador de codigo. ( visual studio code, sublime o el que mejor te vaya).**
   
   > Abrir visual studio ----> apartado extensiones ----> Prettier code formater (standar) ---> Install--->Escoja la configuración de **.JSON** 
     desde aquí https://prettier.io/docs/en/configuration.html y siga la sigueinte configuración:
     
     {
      "trailingComma": "es5",
      "tabWidth": 4,
      "semi": false,
      "singleQuote": true
     }
     
**2. Sigue al Scrum Master en todo momento**

**3. Clean Code es tu lema en este equipo.** 
   
#### Sigue el siguiente esquema de codigo

   - Número de espacios en el codigo es de 4 puntos
   * "camelCase" para llamadas en obejetos y codigo en general (todo en ingles)
   * Seguir siempre las tareas del Scrum
   + "Const" en vez de "Var"


### Autores ✒️

**María Fernandez** - **Mohamed Saadi** - **Jose Ignacio Alvujar** 

### Licencia 📄

**Este proyecto está bajo la Licencia (SaladDays) - mira el archivo [LICENSE.md](LICENSE.md) para detalles**

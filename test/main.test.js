//const { createActivityUrl } = require('../main.js')

const state = {
    display: 'location',
    activityUrl: 'http://www.boredapi.com/api/activity?type=',
    
}

function createActivityUrl(typeActivity) {
    let urlActivityFinal = state.activityUrl + typeActivity
    return urlActivityFinal
}

test('Comprobar que se crea una url válida', function() {
    expect(createActivityUrl('social&type=recreational&type=relaxation')).toBe(
        'http://www.boredapi.com/api/activity?type=social&type=recreational&type=relaxation'
    )
})

/*
test ("Comprobar que se crea una url válida", function () {
    expect(createActivityUrl("Sunny")).toBe("http://www.boredapi.com/api/activity?type=social&type=recreational&type=relaxation")
    })
    
})*/

//test for see the url in weather
/*it('return buttonLocation if receive callWeather', () => {
    expect(callWeather(buttonLocation)).toBe(
        'http://api.weatherstack.com/current?access_key=0a8b7234f3fee003128b3cbed7c15054&query=' +
            location
    )
})*/

function changeState (state, stateToChange) {
state.display = stateToChange
return state.display
}

test('cambia estado', function () {
    let state = {display: null}
    changeState(state, 'Winter')
    expect(state.display).toBe('Winter')
})



// test('añade key único al state.activity', function () {
// const state = {
//    activityData: null, 
//    activity: {
//     name: null,
//     key: ['']
// }

// })